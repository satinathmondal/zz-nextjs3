/**
 * TODO
 */
import { expect } from "chai";
import hello from "../pages/api/hello";
import * as sinon from "ts-sinon";
import { NextApiRequest, NextApiResponse } from "next";
import { Test } from "mocha";
import handler from "../pages/api/hello";

describe("Greeting", () => {
  describe("greeting.hello()", () => {
    it("should return Hello World", () => {
      const req = sinon.stubInterface<NextApiRequest>();
      const res = sinon.stubInterface<NextApiResponse>();

      res.status.alwaysCalledWith(200);
      res.status.returns(res);
      handler(req, res);

      expect(res.status.called).to.be.true;
      expect(res.json.called).to.be.true;
      expect(res.json.args[0][0]).to.deep.equal({ response: "Hello World" });
    });
  });
});
