apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "template-me-nextjs.fullname" . }}
  labels:
    {{- include "template-me-nextjs.labels" . | nindent 4 }}
  {{- with .Values.deployment.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "template-me-nextjs.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.deployment.pod.annotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "template-me-nextjs.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: {{ include "template-me-nextjs.container" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 3000
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
              #initialDelaySeconds: 120
              #periodSeconds: 5
          readinessProbe:
            httpGet:
              path: /
              port: http
              #initialDelaySeconds: 30
              #periodSeconds: 5
          resources:
            {{- toYaml .Values.resources | nindent 12 }}