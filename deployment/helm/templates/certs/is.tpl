{{- if .Values.ingressSecret.enabled }}
apiVersion: di.graingercloud.com/v1
kind: IngressSecret
metadata:
  name: {{ .Values.ingressSecret.certName }}
spec:
  cert: {{ .Values.ingressSecret.publicCert }}
  path_to_key: 
    {{- .Values.team -}}
    /
    {{- .Values.environment -}}
    /
    {{- include "template-me-nextjs.fullname" . -}}
    /tlscert
{{- end }}