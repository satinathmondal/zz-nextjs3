{{- if .Values.gateway.enabled }}
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: {{ include "template-me-nextjs.fullname" . }}
  labels:
    {{- include "template-me-nextjs.labels" . | nindent 4 }}
spec:
  hosts:
  - {{ .Values.fqdn }}
  gateways:
  - {{ include "template-me-nextjs.fullname" . }}
  http:
  - route:
    - destination:
        port:
          number: 80
        host: {{ include "template-me-nextjs.fullname" . }}
{{- end}}