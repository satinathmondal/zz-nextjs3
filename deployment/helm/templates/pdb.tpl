{{- if and .Values.autoscaling.enabled (gt (.Values.autoscaling.minReplicas | int) 1) }}
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: {{ include "template-me-nextjs.fullname" . }}
  labels:
    {{- include "template-me-nextjs.labels" . | nindent 4 }}
spec:
  minAvailable: {{ .Values.autoscaling.minAvailable }}
  selector:
    matchLabels:
      {{- include "template-me-nextjs.selectorLabels" . | nindent 6 }}
{{- end }}