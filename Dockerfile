FROM node:14-alpine AS builder

WORKDIR /app
COPY . .
RUN yarn install --frozen-lockfile && \
    yarn validate:build


FROM node:14-alpine AS runner
WORKDIR /app
ENV NODE_ENV production

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001


COPY --from=builder --chown=nextjs:nodejs /app/cypress ./
COPY --from=builder --chown=nextjs:nodejs /app/.next ./.next
COPY --from=builder --chown=nextjs:nodejs /app/node_modules ./node_modules
COPY --from=builder --chown=nextjs:nodejs /app/next.config.js ./
COPY --from=builder --chown=nextjs:nodejs /app/package.json ./package.json
COPY --from=builder --chown=nextjs:nodejs /app/cypress.json ./cypress.json
USER nextjs
EXPOSE 3000


ENV NEXT_TELEMETRY_DISABLED 1

CMD ["yarn", "start"]