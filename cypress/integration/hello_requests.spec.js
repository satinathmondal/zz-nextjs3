/// <reference types="cypress" />

context("Network Requests", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("cy.request() with query to /api/hello API ", () => {
    // will execute request

    cy.request({
      url: "/api/hello",
    })
      .its("body")
      .should("contain", {
        response: "Hello World",
      });
  });
});
